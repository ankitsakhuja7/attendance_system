'''
Created on Jul 30, 2018

@author: ankit-axiom
'''
from imutils import face_utils, resize
import numpy  as np
import cv2
import dlib


# define a dictionary that maps the indexes of the facial
# landmarks to specific face regions
'''
FACIAL_LANDMARKS_IDXS = OrderedDict([
    ("mouth", (48, 68)),
    ("right_eyebrow", (17, 22)),
    ("right_eye", (36, 42)),
    ("left_eye", (42, 48)),
    ("nose", (27, 36)),
    ("jaw", (0, 17))
])
'''
def rect_to_bb(rect):
    # take a bounding predicted by dlib and convert it
    # to the format (x, y, w, h) as we would normally do
    # with OpenCV
    x = rect.left()
    y = rect.top()
    w = rect.right() - x
    h = rect.bottom() - y

    # return a tuple of (x, y, w, h)
    return (x, y, w, h)

def shape_to_np(shape, dtype="int"):
    # initialize the list of (x, y)-coordinates
    coords = np.zeros((68, 2), dtype=dtype)

    # loop over the 68 facial landmarks and convert them
    # to a 2-tuple of (x, y)-coordinates
    for i in range(0, 68):
        coords[i] = (shape.part(i).x, shape.part(i).y)

    # return the list of (x, y)-coordinates
    return coords

detector= dlib.get_frontal_face_detector()
#Facial landmarks with dlib, OpenCV, and PythonPython


# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
#detector = dlib.get_frontal_face_detector()
#predictor = dlib.shape_predictor(args["shape_predictor"])

# load the input image, resize it, and convert it to grayscale
cap=cv2.VideoCapture("nvcamerasrc ! video/x-raw(memory:NVMM), width=700, height=(int)500, format=(string)I420, framerate=(fraction)240/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink")
#gray= np.zeros((256,256,3), dtype= 'uint8')
#if not cap==True:
 #   print('Failed to open camera')
#else:
count=0
while(True):
    ret,frame=cap.read()
      
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

#image = cv2.imread('demo.jpg')
#image = resize(image, width=250)
#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# detect faces in the grayscale image
    rects = detector(gray,0)
    
 


   

# loop over the face detections
    for (i, rect) in enumerate(rects):
         # determine the facial landmarks for the face region, then
    # convert the facial landmark (x, y)-coordinates to a NumPy
    # array
    #  shape = predictor(gray, rect)
    # shape = face_utils.shape_to_np(shape)
 
    # convert dlib's rectangle to a OpenCV-style bounding box
    # [i.e., (x, y, w, h)], then draw the face bounding box
        (x, y, w, h) = face_utils.rect_to_bb(rect)
        #cv2.rectangle(frame, (x, y-50), (x + w, y + h+20), (0, 0, 0), 1) # show the face number
       # cv2.putText(frame, "Face #{}".format(i + 1), (x - 10, y - 10),
       #             cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        crop_img=frame[y-50:y + h+20,x:x + w]
        if count<=1000:
           # res=cv2.resize(crop_img, dsize=(50,50),interpolation=cv2.INTER_CUBIC)
            cv2.imwrite("/home/nvidia/Documents/thesis/Image/jeevan/Image#{}".format(count+1)+".jpg",crop_img)
            count+=1
       

    # loop over the (x, y)-coordinates for the facial landmarks
    # and draw them on the image
    #for (x, y) in shape:
    #    cv2.circle(image, (x, y), 1, (0, 0, 255), -1)

 
# show the output image with the face detections + facial landmarks
# show the frame
          
    cv2.imshow("Frame", frame)

       
    # if the `q` key was pressed, break from the loop
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q") or count==1000:
        break

cv2.destroyAllWindows()
cap.stop()






























