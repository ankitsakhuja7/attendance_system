'''
Created on Sep 26, 2018

@author: ankit
'''

import keras
from keras.utils import to_categorical


import scipy
import sklearn
import matplotlib.pyplot as plt

from keras.preprocessing import image
from keras.preprocessing.image import load_img, img_to_array

from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras import activations
import numpy as np
from keras.layers.pooling import MaxPool2D
from keras.layers.core import Dropout, Flatten, Dense
from keras.applications.imagenet_utils import preprocess_input

import bcolz
#import path
def save_array(fname, arr): c=bcolz.carray(arr, rootdir=fname, mode='w'); c.flush()
def load_array(fname): return bcolz.open(fname)[:]

#image.ImageDataGenerator()
def imageDataGenerator_func():
    return image.ImageDataGenerator(      
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        shear_range=0.2,
        zoom_range=0.2,
       rescale=1./255
       )
def get_batches(dirname, gen=imageDataGenerator_func(), shuffle=True, batch_size=4, class_mode='categorical',
                target_size=(224,224)):
    return gen.flow_from_directory(dirname, target_size=target_size,
            class_mode=class_mode, shuffle=shuffle, batch_size=batch_size)

def get_data(path, target_size=(224,224)):
    batches = get_batches(path, shuffle=False, batch_size=1, class_mode=None, target_size=target_size)
    return np.concatenate([batches.next() for i in range(batches.samples)])

def preprocess_images(image_path,targetsize):
    img=load_img(image_path,targetsize)
    img=img_to_array(img)
    img=np.expand_dims(img,axis=0)
    img=preprocess_input(img)
    return img




trainpath='/home/ankit/Documents/testProject/data /train'
validationpath='/home/ankit/Documents/testProject/data /test'
#sampleImage_colord='/home/ankit/spyderProject/dataset/cats_dogs/x.jpg'
#sampleImage_grayscale='/home/ankit/spyderProject/Image_agumentation/dataset/test/akash/20180723_120820_001.jpg'

traindatagen= image.ImageDataGenerator(      
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        shear_range=0.2,
        zoom_range=0.2,
       rescale=1./255
       )

validationdatagen=image.ImageDataGenerator(rescale=1./255)

batch_size=10
targetsize=(96,96)
class_mode='categorical'
trainBatch =traindatagen.flow_from_directory(trainpath,batch_size=batch_size, target_size=targetsize,class_mode=class_mode)

validationbatches=validationdatagen.flow_from_directory(validationpath,target_size=targetsize,batch_size=batch_size,class_mode=class_mode)

#trainBatch_from_getBatch_function=get_batches(trainpath, traindatagen, batch_size, class_mode, target_size=(244,244))
#validationBatch_from_getBatch_function=get_batches(validationpath, batch_size, class_mode, target_size=(244,244))

trn_data=get_data(trainpath,target_size=targetsize)
val_data=get_data(validationpath,target_size=targetsize)

save_train_data=save_array('/home/ankit/Documents/testProject/FILES/bcolzfile/96*96/saved_train_data.bc', trn_data)
save_val_data=save_array('/home/ankit/Documents/testProject/FILES/bcolzfile/96*96/saved_val_data.bc', val_data)

print('data_saves')

#loaded_val_data=load_array('saved_val_data.bc')

#loaded_trn_data=load_array('saved_train_data.bc')

#trainNumpyArray=np.array(trainBatch)
#Path="/home/ankit/spyderProject/Image_agumentation/"
#save_array(Path+'train_data.bc', trainBatch)
#trainLoadedArray=load_array('train_data.bc')

#print(trainBatch.classes)
#print(validationbatches.classes)


#print(trainBatch.classes)
#print(validationbatches.classes)
#loaded_trn_data.
'''
trainLabels= to_categorical( trainBatch.classes)
validationLabels=to_categorical(validationbatches.classes)

#x=load_img(sampleImage_grayscale)
#x=img_to_array(x)
#x=x.reshape((1,)+x.shape)

input_shape=trainBatch.image_shape
model=Sequential()

model.add(Conv2D(filters=64,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu',input_shape=input_shape))
model.add(Conv2D(filters=128,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Dropout(rate=0.2))
model.add(Flatten())
model.add(Dense(6,activation='softmax'))
model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
#model.fit_generator(trainBatch, steps_per_epoch=1, epochs=1, validation_data=validationbatches)
model.fit(loaded_trn_data,trainLabels,batch_size=2,validation_data=(loaded_val_data,validationLabels),epochs=5)
train_loss,train_accuracy=model.evaluate(loaded_trn_data, trainLabels, verbose=0)


print('train data losst', train_loss)

print('train data accuracy', train_accuracy)

'''

'''
i=0

batch =traindatagen.flow(x,batch_size=1,save_to_dir='/home/ankit/spyderProject/Image_agumentation/preview')
  #  i+=1
  #  if i>20:
 #       break
'''
