'''
Created on Oct 2, 2018

@author: ankit
'''
import cv2

import multiprocessing

def camera1(_):
    video0="nvcamerasrc ! video/x-raw(memory:NVMM), width=720, height=(int)526, format=(string)I420, framerate=(fraction)60/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"
    cap1=cv2.VideoCapture(video0)
    face_cascade=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
   
    print('camera 1',cap1.isOpened)
    while True:
            ret,frame=cap1.read()
            gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            faces= face_cascade.detectMultiScale(gray, 1.3, 5)
            
            for (x,y,w,h) in faces:
                cv2.rectangle(frame, (x,y),(x+y,y+h),(255,0,0),2)
               # roi_gray=gray[y:y+h,x:x+w]
               # roi_color=img[y:y+h,x:x+w]
                
            cv2.imshow('frame',frame)
            
            if cv2.waitKey(1) &0xFF ==ord('q'):
                break
            
    cap1.release()
    cv2.destroyAllWindows()



def camera2(_):
    video1='/dev/video1'
    cap2=cv2.VideoCapture(video1)
    print('camera 2',cap2.isOpened)
    face_cascade=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
   
    print('camera 1',cap2.isOpened)
    while True:
            ret,frame=cap2.read()
            gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            faces= face_cascade.detectMultiScale(gray, 1.3, 5)
            
            for (x,y,w,h) in faces:
                cv2.rectangle(frame, (x,y),(x+y,y+h),(255,0,0),2)
               # roi_gray=gray[y:y+h,x:x+w]
               # roi_color=img[y:y+h,x:x+w]
                
            cv2.imshow('frame',frame)
            
            if cv2.waitKey(1) &0xFF ==ord('q'):
                break
        
    cap2.release()
    cv2.destroyAllWindows()


arr=[1,2]

p1=multiprocessing.Process(target=camera1,args=(arr,))
p2=multiprocessing.Process(target=camera2, args=(arr,))



p1.start()
p2.start()

p1.join()
p2.join()
 
