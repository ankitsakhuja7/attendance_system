#!/usr/bin/env python
# coding: utf-8

# In[1]:


import scipy
import sklearn
import matplotlib.pyplot as plt
from keras.utils import to_categorical

from keras.preprocessing import image
from keras.preprocessing.image import load_img, img_to_array

from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras import activations
import numpy as np
from keras.layers.pooling import MaxPool2D
from keras.layers.core import Dropout, Flatten, Dense
from keras.optimizers import Adam
from keras.layers import BatchNormalization

import bcolz


# In[2]:


import tensorflow as tf
from keras.backend.tensorflow_backend import  set_session


# In[3]:


config=tf.ConfigProto()


# In[4]:


config.gpu_options.allow_growth=True


# In[5]:


#import path
def save_array(fname, arr): c=bcolz.carray(arr, rootdir=fname, mode='w'); c.flush()
def load_array(fname): return bcolz.open(fname)[:]

def get_batches(dirname, gen=image.ImageDataGenerator(), shuffle=True, batch_size=4, class_mode='categorical',
                target_size=(224,224)):
    return gen.flow_from_directory(dirname, target_size=target_size,
            class_mode=class_mode, shuffle=shuffle, batch_size=batch_size)

def get_data(path, target_size=(96,96)):
    batches = get_batches(path, shuffle=False, batch_size=1, class_mode=None, target_size=target_size)
    return np.concatenate([batches.next() for i in range(batches.samples)])


# In[6]:




trainpath='/home/nvidia/Documents/thesis/ImageData/onlyfaces/train'
validationpath='/home/nvidia/Documents/thesis/ImageData/onlyfaces/test'

#trainpath='/home/akash/miniconda3/Project/data /test'
#validationpath='/home/akash/miniconda3/Project/data /test'

#sampleImage_colord='/home/ankit/spyderProject/dataset/cats_dogs/x.jpg'
#sampleImage_grayscale='/home/ankit/spyderProject/Image_agumentation/dataset/test/akash/20180723_120820_001.jpg'


# In[7]:


# traindatagen= image.ImageDataGenerator(      
#         rotation_range=20,
#         width_shift_range=0.2,
#         height_shift_range=0.2,
#         horizontal_flip=True,
#         shear_range=0.2,
#         zoom_range=0.2,
#        rescale=1./255
#        )

traindatagen= image.ImageDataGenerator(rescale=1./255)

validationdatagen=image.ImageDataGenerator(rescale=1./255)
nb_samples=1254
batch_size=5
targetsize=(96,96)
class_mode='categorical'
trainBatch =traindatagen.flow_from_directory(trainpath,batch_size=batch_size, target_size=targetsize,class_mode=class_mode)

validationbatches=validationdatagen.flow_from_directory(validationpath,target_size=targetsize,batch_size=batch_size,class_mode=class_mode)
loaded_val_data=load_array('/home/nvidia/Documents/thesis/bcolz/onlyfaces/96*96/saved_val_data.bc')

loaded_trn_data=load_array('/home/nvidia/Documents/thesis/bcolz/onlyfaces/96*96/saved_train_data.bc')


# In[8]:


#trainNumpyArray=np.array(trainBatch)
#Path="/home/ankit/spyderProject/Image_agumentation/"
#save_array(Path+'train_data.bc', trainBatch)
#trainLoadedArray=load_array('train_data.bc')

#print(trainBatch.classes)
#print(validationbatches.classes)


#print(trainBatch.classes)
#print(validationbatches.classes)
#loaded_trn_data.
trainLabels= to_categorical( trainBatch.classes)
validationLabels=to_categorical(validationbatches.classes)

#x=load_img(sampleImage_grayscale)
#x=img_to_array(x)
#x=x.reshape((1,)+x.shape)


#print('train data losst', train_loss)

#print('train data accuracy', train_accuracy)



'''
i=0

batch =traindatagen.flow(x,batch_size=1,save_to_dir='/home/ankit/spyderProject/Image_agumentation/preview')
  #  i+=1
  #  if i>20:
 #       break
'''


# In[9]:


input_shape=trainBatch.image_shape
model=Sequential()

model.add(Conv2D(filters=32,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu',input_shape=input_shape))

model.add(Conv2D(filters=32,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))



model.add(MaxPool2D(pool_size=(2,2)))
model.add(Conv2D(filters=64,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))
#model.add(BatchNormalization())
model.add(Conv2D(filters=64,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))



model.add(MaxPool2D(pool_size=(2,2)))
model.add(Conv2D(filters=128,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))

model.add(Conv2D(filters=128,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))



model.add(MaxPool2D(pool_size=(2,2)))


model.add(Dropout(rate=0.4))

model.add(Flatten())
model.add(Dense(6,activation='softmax'))
adam=Adam(lr=0.001)
model.compile(loss='categorical_crossentropy',optimizer=adam,metrics=['accuracy'])
model.fit_generator(trainBatch, steps_per_epoch=15, epochs=15, validation_data=validationbatches,validation_steps=15)
#model.fit(loaded_trn_data,trainLabels,batch_size=batch_size,validation_data=(loaded_val_data,validationLabels),epochs=10)
validation_loss,validation_accuracy=model.evaluate_generator(validationbatches, 1254/5,workers=5)

print('validation_loss',validation_loss)
print('validation_accuracy',validation_accuracy)
model.summary()

model.save('/home/nvidia/Documents/thesis/Models/NewImages/96*96/model.h5')


# In[ ]:




