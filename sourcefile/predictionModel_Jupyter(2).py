#!/usr/bin/env python
# coding: utf-8

# In[1]:


import keras
from keras.preprocessing import image
from keras.preprocessing.image import load_img, img_to_array
from keras.applications.imagenet_utils import preprocess_input
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPool2D
from keras.layers.core import Dropout, Flatten, Dense
from keras.models import load_model
import numpy as np


# In[2]:


targetsize=(96,96)


# In[3]:


def preprocess_images(image_path):
    targetsize=(96,96)
    img=load_img(image_path,target_size=targetsize)
    img=img_to_array(img)
 
  
    img=np.expand_dims(img,axis=0)
    img=preprocess_input(img)
    return img


# In[4]:


def createModel():
    
    model=Sequential()
    model.add(Conv2D(filters=64,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu',input_shape=(96,96,3)))
    model.add(Conv2D(filters=64,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))
    model.add(MaxPool2D(pool_size=(2,2)))

    model.add(Conv2D(filters=128,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))
    model.add(Conv2D(filters=128,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
    
    model.add(Conv2D(filters=256,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))
    model.add(Conv2D(filters=256,strides=(1,1),kernel_size=(3,3),padding='valid',activation='relu'))
    model.add(MaxPool2D(pool_size=(2,2)))
   
    model.add(Dropout(rate=0.4))
    
    model.add(Flatten())
    model.add(Dense(6,activation='softmax'))
    return model


# In[5]:


def load_trained_model():
        weights_path='/home/nvidia/Documents/thesis/Models/96*96/0.97/model.h5'
        model = createModel()
        return load_weights(weights_path)


# In[6]:


def PredictionWeights():
    image_path='/home/nvidia/Documents/thesis/IMG_20181019_092547.jpg'
    x=preprocess_images(image_path)
    loadedModel=load_trained_model()
    loadedModel.predict(x)


# In[7]:


def PredictModel():
    targetsize=(96,96)
    image_path='/home/nvidia/Documents/thesis/IMG_20181019_092547.jpg'
    #resize=image.resize((96,96),)
    x=preprocess_images(image_path=image_path)
    
    
   # Model=createModel()
    loadedModel=load_model('/home/nvidia/Documents/thesis/Models/96*96/0.97/model.h5')
    loadedModel.predict_classes(x)


# In[8]:


Prediction=PredictModel()


# In[9]:


print(Prediction)


# In[ ]:




